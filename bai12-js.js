
// Bài 12:
//dung for
function filterProductByQuality(listProduct) {
    let products = [];
    for (let i = 0; i < listProduct.length; i++) {
        if (listProduct[i]['quality'] > 0 && listProduct[i]['isDelete'] === false) {
            products.push(listProduct[i]);
        }
    }
    return products;
}

//dung es6
const getProductByQuality = (listProduct) => {
    return listProduct.filter(product => product.quality > 0 && product.isDelete === false)
}