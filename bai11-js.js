//Bài 11:

//vong for
function filterProductById(listProduct, idProduct) {
    for (let i = 1; i <= listProduct.length; i++) {
        if (listProduct[i]['id'] === idProduct) {
            return listProduct[i]['name'];
        }
    }
}

//es6
const getProductNameById = (listProduct, idProduct) => {
    const product = listProduct.find(product => product.id === idProduct);
    return product ? product.name : null;
}