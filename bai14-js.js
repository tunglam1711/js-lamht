// Bài 14: Viết function trả về tổng số product ( tổng qulity) chưa bị xóa
//khong reducer"
function totalProduct(listProduct) {
    let total = 0;
    for (let i = 0; i < listProduct.length; i++) {
        if (listProduct[i]['isDelete']===false) {
            total=total + listProduct[i]['quality'];
        }
    }
    return total;
}

//dung reducer
const totalProductReduce = (listProduct) => {
    return listProduct.reduce((total, value) => {
        if (value.isDelete) {
            return total + value.quality;
          }
          return total;
    }, 0)
}