BOM (Browser Object Model) là một chuẩn được sử dụng trong JavaScript để truy cập và thao tác với các tính năng của trình duyệt web, chẳng hạn như cửa sổ trình duyệt, địa chỉ URL, lịch sử duyệt web, và các hộp thoại cảnh báo.
BOM bao gồm nhiều đối tượng được tích hợp sẵn trong JavaScript, cho phép chúng ta truy cập và thao tác với các tính năng của trình duyệt web.
Các đối tượng này bao gồm:

    - window: đại diện cho cửa sổ trình duyệt.
    - location: đại diện cho địa chỉ URL hiện tại của trang.
    - history: đại diện cho lịch sử duyệt web.
    - navigator: chứa thông tin về trình duyệt đang được sử dụng.
    - screen: chứa thông tin về kích thước và độ phân giải của màn hình.
Ví dụ, để mở một cửa sổ mới trong trình duyệt, chúng ta có thể sử dụng đối tượng window như sau:
    window.open('https://www.google.com');