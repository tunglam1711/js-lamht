Hàm call() dùng để thực thi một hàm nào đó với các tham số truyền vào (nếu có), hàm này được tích hợp sẵn trong các đối tượng là function.
Ví dụ:
    function myProfile(name, age){
        console.log(name);
        console.log(age);
        return this;
    }

    var person = myProfile.call(myProfile, "Nguyễn Văn A", 27);

Hàm apply() có tác dụng giống call(), nhưng cú pháp khác nhau.Tham số đầu tiên của call() là đối tượng this, tiếp đó là các tham số của hàm cần gọi.
Tham số đầu tiên của hàm apply() là đối tượng this, tiếp theo là 1 mảng chứa các tham số của hàm cần gọi.
Ví dụ:
    var sayHello = function(name, message){
        console.log(message + name);
    };

    sayHello.call(sayHello, 'Lâm', ' Xin chào ');
    sayHello.apply(sayHello, ['Lâm', ' Xin chào ']);