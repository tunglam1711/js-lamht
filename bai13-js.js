// Bài 13:
// dung for
function filterProductBySaleDate(listProduct) {
    let products = [];
    let currentDate = new Date();
    for (let i = 0; i < listProduct.length; i++) {
        if (listProduct[i]['saleDate'].toLocaleDateString() > currentDate.toLocaleDateString() && listProduct[i]['isDelete'] === false) {
            products.push(listProduct[i])
        }
    }
    return products;
}

//dung es6
const getProductBySaleDate = (listProduct) => {
    let currentDate = new Date();
    return listProduct.filter(product => product.saleDate.toLocaleDateString() > currentDate.toLocaleDateString() && product.isDelete === false)
}
