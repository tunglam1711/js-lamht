Hoisting là một tính năng của JavaScript, cho phép các khai báo biến và hàm được di chuyển lên đầu phạm vi của chúng, trước khi thực hiện bất kỳ mã nào trong phạm vi đó.
Tức là, dù khai báo biến hay hàm ở đâu trong phạm vi, chúng đều được coi là được khai báo ở đầu phạm vi đó.

Tuy nhiên, chỉ có khai báo biến và hàm được hoisting, còn các biểu thức gán giá trị cho biến thì không được hoisting.
Do đó, nếu một biến được sử dụng mà trước đó chưa được khai báo bằng var, let hoặc const, thì sẽ xảy ra lỗi "ReferenceError".
Ví dụ: console.log(x); // Output: undefined
       var x = 5;