Callback trong JavaScript là một hàm được truyền vào một hàm khác như một tham số, và được gọi lại (execute) khi hàm chính hoàn thành nhiệm vụ của nó.
Callback thường được sử dụng trong các tình huống xử lý bất đồng bộ (asynchronous) như AJAX, event handling, và setTimeout.
Ví dụ:
    function A(){
       // code
    }

    // Hàm B có một tham số callback
    function B(callback){
        callback();
    }

    // Gọi hàm B và truyền tham số là hàm A
    B(A);