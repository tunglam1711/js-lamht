//Bài 16:

//dung for
function filterProductBySaleDate(listProduct) {
    let products = [];
    let currentDate = new Date();
    for (let i = 0; i < listProduct.length; i++) {
        if (listProduct[i].saleDate.toLocaleDateString() > currentDate.toLocaleDateString() && listProduct[i].quality >0) {
            products.push([listProduct[i]['id'],listProduct[i]['name']])
        }
    }
    return products;
}

//dung es6
const getProductBySaleDate = (listProduct) => {
    let currentDate = new Date();
    return listProduct.filter(product => product.saleDate.toLocaleDateString() > currentDate.toLocaleDateString() && product.quality > 0)
        .map(product =>[product.id,product.name])
}
